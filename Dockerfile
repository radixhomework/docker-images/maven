FROM maven:3.6.3-openjdk-11

RUN apt-get update && apt-get -y install genisoimage

ENTRYPOINT ["/usr/local/bin/mvn-entrypoint.sh"]
CMD ["mvn"]